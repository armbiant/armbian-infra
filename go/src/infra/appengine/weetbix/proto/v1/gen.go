// Copyright 2021 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package weetbixpb

//go:generate cproto
//go:generate svcdec -type RulesServer
//go:generate svcdec -type ProjectsServer
//go:generate svcdec -type InitDataGeneratorServer
//go:generate svcdec -type ClustersServer
//go:generate svcdec -type TestHistoryServer
//go:generate svcdec -type TestVariantsServer
