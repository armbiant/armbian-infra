// Copyright 2022 The ChromiumOS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package dumper

import (
	"context"
	"fmt"
	"reflect"
	"time"

	"github.com/golang/protobuf/proto"
	"go.chromium.org/luci/common/errors"
	"go.chromium.org/luci/common/logging"

	ufsmfgcfg "infra/unifiedfleet/api/v1/models/chromeos/manufacturing"
	"infra/unifiedfleet/app/controller"
	"infra/unifiedfleet/app/model/registration"
	"infra/unifiedfleet/app/util"
)

// manufacturingConfigDiffHandler compares the ManufacturingConfig fetched from
// Inventory V2 with the ManufacturingConfig generated by parsing HwidData
// stored in UFS.
func manufacturingConfigDiffHandler(ctx context.Context) error {
	ctx = setupMfgcfgDiffContext(ctx)

	// File writer setup
	filename := fmt.Sprintf("mfgcfg_diff/%s.log", time.Now().UTC().Format("2006-01-02T03:04:05"))
	writer, err := getCloudStorageWriter(ctx, filename)
	if err != nil {
		return err
	}
	defer func() {
		if writer != nil {
			if err := writer.Close(); err != nil {
				logging.Warningf(ctx, "failed to close cloud storage writer: %s", err)
			}
		}
	}()

	// Set up clients for Inv v2 and HWID server
	invV2Client, err := controller.GetInventoryV2Client(ctx)
	if err != nil {
		return err
	}

	hwidClient, err := controller.GetHwidClient(ctx)
	if err != nil {
		return err
	}

	// Get all machines from UFS datastore
	machines, err := registration.ListAllMachines(ctx, false)
	if err != nil {
		return err
	}
	if machines == nil {
		return errors.New("machine lse entities are missing")
	}

	for _, machine := range machines {
		hwid := machine.GetChromeosMachine().GetHwid()
		if hwid == "" {
			continue
		}

		// Inv V2 implementation
		mfgCfgInvV2, err := controller.GetManufacturingConfigFromInvV2(ctx, invV2Client, hwid)
		if err != nil {
			logging.Warningf(ctx, "InvV2 ManufacturingConfig for %s not found. Error: %s", hwid, err)
		}

		// UFS implementation
		hwidData, err := controller.GetHwidData(ctx, hwidClient, hwid)
		if err != nil {
			logging.Warningf(ctx, "Hwid data for %s not found. Error: %s", hwid, err)
		}

		var mfgCfgUFS *ufsmfgcfg.ManufacturingConfig
		if !reflect.ValueOf(hwidData).IsNil() {
			mfgCfgUFS, err = controller.GetManufacturingConfigFromUFS(ctx, hwidData)
			if err != nil {
				logging.Warningf(ctx, "UFS ManufacturingConfig for %s not found. Error: %s", hwid, err)
			}
		}

		// Log information on diff if exists
		if !proto.Equal(mfgCfgInvV2, mfgCfgUFS) {
			logMsg := fmt.Sprintf("%s %s", machine.GetName(), hwid)
			if _, err := fmt.Fprint(writer, logMsg); err != nil {
				return err
			}
		}
	}

	return nil
}

func setupMfgcfgDiffContext(ctx context.Context) context.Context {
	ctx = logging.SetLevel(ctx, logging.Warning)
	ctx, _ = util.SetupDatastoreNamespace(ctx, util.OSNamespace)
	return ctx
}
