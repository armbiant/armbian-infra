// Copyright 2022 The ChromiumOS Authors.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
package main

const (
	Success             = 0
	AuthError           = 1
	BBError             = 2
	NotImplementedError = 3
)
